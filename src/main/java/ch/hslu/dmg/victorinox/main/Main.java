package ch.hslu.dmg.victorinox.main;

/**
 * Fragt den Benutzer was er machen möchte und führt seine Anweisungen aus.
 *
 * @author User
 */
public final class Main {

    /**
     * Private Konstruktor weil Main-Methode.
     */
    private Main() {
    }

    /**
     * Startet das Programm und fragt den Benutzer nach Anweisungen.
     *
     * @param args
     */
    public static void main(String[] args) {
        final Main main = new Main();
        while (true) {
            Console.displayMenu();
            final char option = Console.getOptionFromUser();
            main.chooseNextStep(option);
        }
    }

    /**
     * Wählt den nächsten Schritt je nachdem was der Benutzer gewählt hat.
     *
     * @param option Vom Benutzer gewählte Option
     */
    public void chooseNextStep(final char option) {
        switch (option) {
            case 'A':
                new Order().placeOrder();
                break;

            case 'B':
                Fertigungsplan.printFertigungsplan();
                break;
            case 'C':
                DbConnection.configureDatabase();
                break;
            case 'D':
                System.exit(0);
                break;
            default:
                System.out.println("Option exisitert nicht");
                break;
        }
    }

}
