package ch.hslu.dmg.victorinox.main;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.impl.JDBCASCIITableAware;
import com.bethecoder.ascii_table.spec.IASCIITableAware;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Fertigungsplan {

    private Fertigungsplan() {
    }

    public static void printFertigungsplan() {
        try (Connection connection = DbConnection.getConnection();) {
            String query = "SELECT"
                    + " fertigungsschritt.fertigungsdatum AS Datum, "
                    + " teil.bezeichnung AS Teil,"
                    + " maschine.bezeichnung AS Maschine,"
                    + " mitarbeiter.name AS Mitarbeiter"
                    + " FROM"
                    + "    fertigungsschritt"
                    + "        INNER JOIN"
                    + "    teil ON fertigungsschritt.teil_id = teil.teil_id"
                    + "        INNER JOIN"
                    + "    maschine ON fertigungsschritt.maschine_id = maschine.maschine_id"
                    + "        INNER JOIN"
                    + "    mitarbeiter ON fertigungsschritt.mitarbeiter_id = mitarbeiter.mitarbeiter_id"
                    + " ORDER BY Datum DESC";
            IASCIITableAware asciiTableAware = new JDBCASCIITableAware(
                    connection, query);
            ASCIITable.getInstance().printTable(asciiTableAware);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
