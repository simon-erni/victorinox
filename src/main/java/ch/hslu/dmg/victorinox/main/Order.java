package ch.hslu.dmg.victorinox.main;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Order {

    public void placeOrder() {
        Console.printBox("Bestellung aufgeben");
        InputStream input = System.in;
        final int kunde = Console.getIntFromUser(input, "Kunden-ID");
        final int teilID = Console.getIntFromUser(input, "Teil-ID");

        Date lieferDatum = planTeile(teilID);

        try (Connection connection = DbConnection.getConnection()) {

            Statement statement = connection.createStatement();
            String query = "INSERT INTO "
                    + "bestellung(teil_id, kunde, liefertermin) "
                    + "VALUES ("
                    + teilID + ","
                    + kunde + ","
                    + "'" + formateDateForSQL(lieferDatum) + "')";
            statement.execute(query);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Date planTeile(final int teil) {
        int menge = 1;
        List<Integer> unterteile = getUnterteile(teil);
        Date startDate = onlyDay(new Date());
        if (unterteile.size() > 0) {
            for (int unterteil : unterteile) {
                Date tempDate = planTeile(unterteil);
                if (tempDate.compareTo(startDate) > 0) {
                    startDate = tempDate;
                }
            }
            return insertFertigungsschritt(startDate, teil, menge);
        }

        return startDate;
    }

    private List<Integer> getUnterteile(final int oberteil) {
        LinkedList<Integer> list = new LinkedList<>();
        try (Connection connection = DbConnection.getConnection();) {
            Statement statement = connection.createStatement();
            String query = "SELECT unterteil FROM stückliste WHERE oberteil='" + oberteil + "'";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    private int getMaschine(final int teil) {
        int maschinenID = 0;
        try (Connection connection = DbConnection.getConnection()) {
            Statement statement = connection.createStatement();
            String query = "SELECT maschine_id FROM fertigungsteil WHERE teil_id='" + teil + "'";
            ResultSet result = statement.executeQuery(query);
            result.next();
            maschinenID = result.getInt(1);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maschinenID;
    }

    private Date insertFertigungsschritt(final Date startDate, final int teil_id, final int menge) {
        Date produktionsDatum = startDate;
        final int maschineId = getMaschine(teil_id);
        final List<Integer> mitarbeiter = getMitarbeiter(maschineId);
        boolean found;
        int mitarbeiterID = 0;
        do {

            produktionsDatum = getFreeMaschineDate(produktionsDatum, maschineId);
            try {
                mitarbeiterID = getFreeMitarbeiter(produktionsDatum, mitarbeiter);
                found = true;
            } catch (NoSuchFieldException ex) {
                found = false;
                produktionsDatum = addDays(produktionsDatum, 1);
            }

        } while (!found);

        try (Connection connection = DbConnection.getConnection()) {

            Statement statement = connection.createStatement();
            String query = "INSERT INTO "
                    + "fertigungsschritt(teil_id, mitarbeiter_id, maschine_id, fertigungsdatum, menge) "
                    + "VALUES ("
                    + teil_id + ","
                    + mitarbeiterID + ","
                    + maschineId + ","
                    + "'" + formateDateForSQL(produktionsDatum) + "',"
                    + menge + ")";
            statement.execute(query);
            return produktionsDatum;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Date getFreeMaschineDate(final Date startDate, final int maschine_id) {
        Date freeDate = onlyDay(startDate);

        try (Connection connection = DbConnection.getConnection()) {
            Statement statement = connection.createStatement();
            // TODO where mit aktuellem Datum
            String query = "SELECT fertigungsdatum FROM fertigungsschritt WHERE maschine_id='" + maschine_id + "' ORDER BY fertigungsdatum ASC";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                Date besetztDatum = onlyDay(result.getDate(1));

                if (besetztDatum.compareTo(freeDate) == 0) {
                    freeDate = addDays(freeDate, 1);
                } else {
                    break;
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        return freeDate;
    }

    private int getFreeMitarbeiter(final Date date, final List<Integer> mitarbeiter) throws NoSuchFieldException {
        String freeDate = formateDateForSQL(date);
        try (Connection connection = DbConnection.getConnection()) {
            for (int mitarbeiterID : mitarbeiter) {
                Statement statement = connection.createStatement();
                String query = "SELECT COUNT(1) FROM fertigungsschritt WHERE mitarbeiter_id='" + mitarbeiterID + "' AND fertigungsdatum='" + freeDate + "'";
                ResultSet result = statement.executeQuery(query);
                result.next();

                // Jetzt ist Mitarbeiter frei
                if (result.getInt(1) == 0) {
                    return mitarbeiterID;
                }

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        throw new NoSuchFieldException("Kein Mitarbeiter verfügbar");
    }

    private Date onlyDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    private Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    private List<Integer> getMitarbeiter(int maschine_id) {
        LinkedList<Integer> list = new LinkedList<>();
        try (Connection connection = DbConnection.getConnection();) {
            Statement statement = connection.createStatement();
            String query = "SELECT mitarbeiter_id FROM kann_bedienen WHERE maschine_id='" + maschine_id + "'";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    private String formateDateForSQL(final Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

}
