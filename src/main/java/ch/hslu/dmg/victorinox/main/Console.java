package ch.hslu.dmg.victorinox.main;

import java.io.InputStream;
import java.util.Scanner;

public class Console {

    private Console() {
    }

    public static String getStringFromUser(InputStream input, String label) {
        Scanner scanner = new Scanner(input);
        System.out.print(label + ": ");

        while (!scanner.hasNext("^[a-zA-Z0-9_]*$")) {
            System.out.println(scanner.next() + " ist keine gültige Eingabe");
            System.out.print(label + ": ");
            scanner = new Scanner(input);
        }

        return scanner.next().toLowerCase();
    }

    public static int getIntFromUser(InputStream input, String label) {
        Scanner scanner = new Scanner(input);
        System.out.print(label + ": ");

        while (!scanner.hasNextInt()) {
            System.out.println(scanner.next() + " ist keine gültige Eingabe");
            System.out.print(label + ": ");
            scanner = new Scanner(input);
        }

        return Integer.valueOf(scanner.next());
    }

    public static void printBox(String title) {
        String border = "+";
        for (int i = 0; i < title.length() + 2; i++) {
            border += "-";
        }
        border += "+";

        System.out.println(border);
        System.out.println("| " + title + " |");
        System.out.println(border);
    }

    /**
     * Zeigt das Hauptmenü an.
     */
    public static void displayMenu() {
        System.out.println("+-----------------------------------+");
        System.out.println("|   FERTIGUNGSPLANUNG VICTORINOX    |");
        System.out.println("+-----------------------------------+");
        System.out.println("|   Optionen:                       |");
        System.out.println("|        A. Bestellung aufgeben     |");
        System.out.println("|        B. Fertigungsplan zeigen   |");
        System.out.println("|        C. Datenbank konfigurieren |");
        System.out.println("|        D. Verlassen               |");
        System.out.println("+-----------------------------------+\n");
    }

    /**
     * Prüft die Eingabe des Benutzers und fragt ihn so lange bis eine gültige
     * Eingabe gemacht wurde.
     *
     * @return Vom Benutzer gewählte Option
     */
    public static char getOptionFromUser() {
        final Scanner scanner = new Scanner(System.in);
        System.out.print("Bitte wähle A, B, C oder D: ");

        while (!scanner.hasNext("[A-Da-d]")) {
            System.out.println(scanner.next() + " ist keine gültige Eingabe");
            System.out.print("Bitte wähle A, B, C oder D: ");
        }

        return scanner.next().toUpperCase().charAt(0);
    }
}
