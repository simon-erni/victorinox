package ch.hslu.dmg.victorinox.main;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.prefs.Preferences;

/**
 *
 * @author User
 */
public class DbConnection {

    private static final String HOST = Preferences.userNodeForPackage(DbConnection.class).get("host", "localhost");
    private static final int PORT = Preferences.userNodeForPackage(DbConnection.class).getInt("port", 3306);
    private static final String DATABASE = Preferences.userNodeForPackage(DbConnection.class).get("database", "victorinox");

    private static final String USER = Preferences.userNodeForPackage(DbConnection.class).get("user", "root");
    private static final String PASSWORD = Preferences.userNodeForPackage(DbConnection.class).get("password", "");

    private DbConnection() {
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = String.format("jdbc:mysql://%1$s:%2$d/%3$s", HOST, PORT, DATABASE);
        
        return DriverManager.getConnection(url, USER, PASSWORD);
    }

    public static void configureDatabase() {
        final InputStream input = System.in;
        Console.printBox("Datenbankverbindung einstellen");

        String host = Console.getStringFromUser(input, "Hostname (" + HOST + ")");
        int port = Console.getIntFromUser(input, "Port (" + PORT + ")");
        String database = Console.getStringFromUser(input, "Datenbank (" + DATABASE + ")");
        String user = Console.getStringFromUser(input, "Benutzername (" + USER + ")");
        String password = Console.getStringFromUser(input, "Passwort");

        savePreferences(host, port, database, user, password);

        System.out.println("Datenbank erfolgreich konfiguriert");
    }

    private static void savePreferences(String host, int port, String database, String user, String password) {
        Preferences preferences = Preferences.userNodeForPackage(DbConnection.class);
        preferences.put("host", host);
        preferences.putInt("port", port);
        preferences.put("database", database);
        preferences.put("user", user);
        preferences.put("password", password);
    }

}
