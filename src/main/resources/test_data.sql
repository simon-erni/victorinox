USE victorinox;

-- mitarbeiter

INSERT INTO mitarbeiter(name, adresse, gehalt, geburtstag) VALUES (
    'Hans Muster',
    'Hauptstrasse 25',
    60000,
    '1958-01-12'
);
INSERT INTO mitarbeiter(name, adresse, gehalt, geburtstag) VALUES (
    'Fabian Wüthrich',
    'Hauptstrasse 30',
    100000,
    '1991-04-24'
);
INSERT INTO mitarbeiter(name, adresse, gehalt, geburtstag) VALUES (
    'Simon Erni',
    'Hauptstrasse 35',
    1000000,
    '1995-11-23'
);
INSERT INTO mitarbeiter(name, adresse, gehalt, geburtstag) VALUES (
    'Alex Sutter',
    'Hauptstrasse 40',
    10000,
    '1999-01-13'
);
INSERT INTO mitarbeiter(name, adresse, gehalt, geburtstag) VALUES (
    'Stefan Winterberger',
    'Hasliberg',
    100000,
    '1960-01-12'
);

-- maschine

INSERT INTO maschine(bezeichnung, wartungsintervall) VALUES (
    'Klingenschleifer 5000',
    1
);
INSERT INTO maschine(bezeichnung, wartungsintervall) VALUES (
    'Drahtverdreher XC300',
    5
);
INSERT INTO maschine(bezeichnung, wartungsintervall) VALUES (
    'Zahnstocherspitzer XYZ',
    3
);
INSERT INTO maschine(bezeichnung, wartungsintervall) VALUES (
    'Schweizerkreuz Stanzer 1291',
    2
);
INSERT INTO maschine(bezeichnung, wartungsintervall) VALUES (
    'Lackiermaschine Red Devil',
    12
);
INSERT INTO maschine(bezeichnung, wartungsintervall) VALUES (
    'Sackmessermaschine Swissnes',
    99
);

-- kann_bedienen

INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    5,
    1
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    4,
    4
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    3,
    3
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    2,
    4
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    1,
    5
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    3,
    1
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    4,
    5
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    2,
    3
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    3,
    5
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    1,
    6
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    2,
    6
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    4,
    6
);
INSERT INTO kann_bedienen(mitarbeiter_id, maschine_id) VALUES (
    5,
    6
);

-- kann_warten

INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    5,
    1
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    4,
    2
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    3,
    3
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    2,
    4
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    1,
    5
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    3,
    1
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    4,
    5
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    2,
    1
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    3,
    4
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    2,
    5
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    4,
    6
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    2,
    6
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    3,
    6
);
INSERT INTO kann_warten(mitarbeiter_id, maschine_id) VALUES (
    5,
    6
);

-- Lieferant

INSERT INTO lieferant(name, adresse) VALUES (
    'Meier Metallverarbeitung AG',
    'Fabrikweg 24'
);
INSERT INTO lieferant(name, adresse) VALUES (
    'Dreher Zahnräder GmbH',
    'Im Rank 15'
);
INSERT INTO lieferant(name, adresse) VALUES (
    'Zahnstocher Fabrik AG',
    'Obere Mühle 1'
);
INSERT INTO lieferant(name, adresse) VALUES (
    'Sharp Knifes AG',
    'Klingenstrasse 500'
);
INSERT INTO lieferant(name, adresse) VALUES (
    'Korkenzieher Müller GmbH',
    'Dorfstrasse 23'
);

-- basisteil

INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Messer gross',
    30.00,
    '20x40x2'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    4
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Messer klein',
    20.00,
    '20x20x2'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    4
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Korkenzieher',
    10.00,
    '10x10x5'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    5
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Zahnstocher',
    1.00,
    '1x1x10'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    3
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Pinzette',
    20.00,
    '2x2x20'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    3
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Zahnrad nano',
    0.40,
    '0.1x0.2x2'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    2
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Werkzeugstahl',
    20.00,
    '20x20x20'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    1
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Chromstahl',
    50.00,
    '20x20x20'
);
INSERT INTO basisteil(teil_id, lieferant_id) VALUES (
    last_insert_id(),
    1
);

-- fertigungsteil

INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Scharfes Messer gross',
    30.00,
    '20x40x2'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    1
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Scharfes Messer klein',
    20.00,
    '2x40x2'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    1
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Spezial Korkenzieher zum Bier öffnen',
    300.00,
    '20x40x2'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    2
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Zahnstocher für Erwachsene',
    35.00,
    '20x600x2'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    3
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Zahnstocher für Kinder',
    31.00,
    '20x6x2'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    3
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Schweizerkreuz Chrom',
    105.00,
    '60x60x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    4
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Schweizerkreuz auf rotem Grund',
    305.00,
    '100x100x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    4
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Aussenverkleidung Rot',
    26.00,
    '100x60x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    5
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Innenverkleidung Chrom',
    104.00,
    '60x60x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    5
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Swiss Champ Sackmesser',
    204.00,
    '60x60x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    6
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Hiker Sackmesser',
    95.00,
    '60x60x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    6
);
INSERT INTO teil(bezeichnung, preis, grössenangabe) VALUES (
    'Classic Sackmesser',
    56.00,
    '60x60x1'
);
INSERT INTO fertigungsteil(teil_id, maschine_id) VALUES (
    last_insert_id(),
    6
);

-- stückliste

INSERT INTO stückliste(oberteil, unterteil) VALUES (
    9,
    1
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    10,
    2
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    11,
    7
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    12,
    4
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    13,
    4
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    14,
    8
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    15,
    8
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    16,
    6
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    17,
    8
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    18,
    9
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    18,
    10
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    18,
    11
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    18,
    12
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    18,
    15
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    18,
    16
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    19,
    10
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    19,
    13
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    19,
    14
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    19,
    17
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    20,
    9
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    20,
    15
);
INSERT INTO stückliste(oberteil, unterteil) VALUES (
    20,
    16
);